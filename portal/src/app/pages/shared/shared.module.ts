import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbTimepickerModule,
  NbUserModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsRoutingModule } from '../forms/forms-routing.module';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';
import { LiveClassComponent } from './live-class/live-class.component';
import { StudentComponent } from './student/student.component';
import { TrainerComponent } from './trainer/trainer.component';
import { SharedRoutingModule } from './shared-routing.module';
import { SharedComponent } from './shared.component';
import { NextClassesComponent } from './next-classes/next-classes.component';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import localePtBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { FindClassComponent } from './find-class/find-class.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';

registerLocaleData(localePtBr);

@NgModule({
  imports: [
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    FormsRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    ReactiveFormsModule,
    NbTimepickerModule,
    
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    
    Ng2SmartTableModule,
    
    SharedRoutingModule,
  ],
  declarations: [
    SharedComponent,
    
    FindClassComponent,
    NextClassesComponent,
    LiveClassComponent,
    StudentComponent,
    TrainerComponent,
  ],
})
export class SharedModule { }
