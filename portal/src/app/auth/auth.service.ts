import { Injectable } from '@angular/core';
import { NbAuthService, NbPasswordAuthStrategy, NbTokenService } from '@nebular/auth';
import { AuthSimpleToken } from './auth-simple-token';
import { RoleProvider } from './role.provider';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends NbAuthService {
  user: any;

  constructor(
    tokenService: NbTokenService,
    private roleProvider: RoleProvider){
    super(tokenService, NbPasswordAuthStrategy);
  }

  getUser(): any {
      this.getToken()
        .subscribe((authSimpleToken: AuthSimpleToken) => {
          if (authSimpleToken.isValid()) {
            this.roleProvider.getRole()
              .subscribe(role => {
                if (role === 'ROLE_TRAINER') {
                  this.user = authSimpleToken.token.trainer;
                }
                else {
                  this.user = authSimpleToken.token.student;
                }
    
                this.user['picture'] = 'assets/images/jack.png';
    
                return this.user;
              });
          }
        });
    }    
}
