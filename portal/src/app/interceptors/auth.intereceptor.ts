import { NbAuthService } from '@nebular/auth';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { AuthSimpleToken } from "../auth/auth-simple-token";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  token: any;

  constructor(private authService: NbAuthService) {
    this.getAuthTokenValue();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
        'Content-Type': 'application/json',
        'Authorization': `${this.token}`,
      },
    });

    return next.handle(req);
  }

  getAuthTokenValue() {
    this.authService.getToken()
      .subscribe((authSimpleToken: AuthSimpleToken) => {
        this.token = authSimpleToken.token.basic_authorization;
      });
  }
}