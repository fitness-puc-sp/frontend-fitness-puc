import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ErrorHandler } from '../../utils/error-handler.utils';
import { Trainer } from '../../models/trainer/trainer';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TrainerService extends ErrorHandler {
  trainerRoute = 'trainer';

  constructor(private http: HttpClient) { 
    super(); 
  }

  saveTrainer(trainer: Trainer): Observable<Trainer> {
    return this.http
      .post<Trainer>(`${environment.apiUrl}/${this.trainerRoute}`, trainer)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }

  updateTrainer(trainer: Trainer): Observable<Trainer> {
    return this.http
      .put<Trainer>(`${environment.apiUrl}/${this.trainerRoute}`, trainer)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }

  deleteTrainer(id: number): Observable<any> {
    return this.http
      .delete(`${environment.apiUrl}/${this.trainerRoute}/${id}`)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }

  //#region "Gets for trainer"
  getTrainerByName(name: string): Observable<any> {
    const url = `${environment.apiUrl}/${this.trainerRoute}/name/${name}`;
    return this.http.get(url);
  }

  getAllTrainers(): Observable<any> {
    const url = `${environment.apiUrl}/${this.trainerRoute}`;
    return this.http.get(url);
  }

  getTrainerById(id: number): Observable<Trainer> {
    const url = `${environment.apiUrl}/${this.trainerRoute}/id/${id}`;
    return this.http.get<Trainer>(url);
  }
  //#endregion
}
