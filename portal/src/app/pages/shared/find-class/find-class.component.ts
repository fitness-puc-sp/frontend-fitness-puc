import { TrainerService } from './../../../services/trainer/trainer.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { NbComponentStatus } from '@nebular/theme';
import { CategoryService } from '../../../services/category/category.service';
import { FieldService } from '../../../services/field/field.service';
import { ToastrComponent } from '../../modal-overlays/toastr/toastr.component';
import * as moment from 'moment';
import { DateHandler } from '../../../utils/date-handler.util';
import { ClassService } from '../../../services/class/class.service';
import { LocalDataSource } from 'ng2-smart-table';
import { AuthSimpleToken } from "../../../auth/auth-simple-token";
import { RoleProvider } from '../../../auth/role.provider';
import { AuthService } from '../../../auth/auth.service';
import { DatetimeRenderComponent } from './renders/datetime-render.component';
import { TrainerRenderComponent } from './renders/trainer-render.component';
import { FieldRenderComponent } from './renders/field-render.component';
import { ButtonViewComponent } from './views/button-view.component';

@Component({
  selector: 'ngx-find-class',
  templateUrl: './find-class.component.html',
  styleUrls: ['./find-class.component.scss']
})
export class FindClassComponent implements OnInit {

  startDateControl = new FormControl(null, [Validators.required]);
  endDateControl = new FormControl(null, [Validators.required]);
  durationControl = new FormControl(null, [Validators.required, Validators.min(5), Validators.max(40)]);
  categoriesControl = new FormControl(null, [Validators.required]);
  fieldsControl = new FormControl(null, [Validators.required]);
  trainersControl = new FormControl(null, [Validators.required]);

  status: NbComponentStatus = 'primary';

  categories: any;
  fields: any;
  trainers: any;

  selectedCategory = '1';
  selectedField = '1';
  selectedTrainer = '0';

  minDateTime: any;

  // #region smartTable settings
  settings: any;

  source: LocalDataSource = new LocalDataSource();
  //#endregion

  userId: any;
  role: any;

  constructor(    
    private classService : ClassService,
    private categoryService: CategoryService,
    private fieldService: FieldService,
    private trainerService: TrainerService,
    private toastrComponent: ToastrComponent,
    private authService: AuthService,
    private roleProvider: RoleProvider) { }
    
  ngOnInit(): void {
    this.minDateTime = DateHandler.getStringNowDateForDateTimeInput();
    this.getCategories();
    this.getTrainers();
    this.getRole();
  }

  configureTableColumnSettings() {debugger;
    this.settings = {
      noDataMessage: "Não foram encontradas aulas",
      actions: {
        add: false,
        edit: false,
        delete: false,
        position: 'right',
      },
      pager: {
        perPage: 5,
      },
      columns: {
        id: {
          title: 'ID',
          type: 'number',
          hide: true,
          editable: false,
        },
        startTime: {
          title: 'Data/Hora início',
          type: 'custom',
          renderComponent: DatetimeRenderComponent,
          editable: false,
        },
        duration: {
          title: 'Duração (mins)',
          type: 'number',
          editable: false,
        },
        trainer: {
          title: 'Professor',
          type: 'custom',
          renderComponent: TrainerRenderComponent,
          editable: false,
        },
        field: {
          title: 'Área',
          type: 'custom',
          renderComponent: FieldRenderComponent,
          editable: false,
        },
        title: {
          title: 'Título',
          type: 'string',
          editable: false,
        },
        type: {
          title: 'Tipo',
          type: 'string',
          editable: false,
        },
        // description: {
        //   title: 'Descrição',
        //   type: 'string',
        //   editable: false,
        // },
        button: {
          title: 'INSCREVA-SE!',
          type: 'custom',
          renderComponent: ButtonViewComponent,
          onComponentInitFunction: (instance) => {
            instance.save.subscribe(classRow => {
              this.subscribeClass(classRow);
            });
          },
          filter: false,
          sort: false,
          // hide: this.role === 'ROLE_STUDENT',
        },
      },
    };
  }

  getRole() {
    this.roleProvider.getRole()
      .subscribe(role => {
        this.role = role;
        this.configureTableColumnSettings();
      });  
  }

  getCategories() {
    this.categoryService.getAllCategories()
      .subscribe(payload => {
        this.categories = payload.categories;

        if (payload.categories.length === 1) {
          this.selectedCategory = payload.categories[0].id;
        }
      });
  }

  getAllFieldsByCategoryId(categoryId: number) {
    this.fieldService.getAllFieldsByCategoryId(categoryId)
      .subscribe(payload => {
        this.fields = payload.fields;

        if (payload.fields.length === 1) {
          this.selectedField = payload.fields[0].id;
        }
      });
  }

  categoryChanged(event:any) {
    this.fields = null;
    this.selectedField = null;
    this.getAllFieldsByCategoryId(+event);
  }

  getTrainers() {
    this.trainerService.getAllTrainers()
      .subscribe(payload => {
        this.trainers = payload.trainers;

        if (payload.trainers.length === 1) {
          this.selectedTrainer = payload.trainers[0].id;
        }
      });
  }

  onStartDateBlur(event) {
    if (this.startDateControl.value && this.isDateBeforeNow(this.startDateControl)) {
      return;
    }
  }

  onEndDateBlur(event) {
    if (this.endDateControl.value && this.isDateBeforeNow(this.endDateControl)) {
      return;
    }
  }

  onDurationBlur(event) {
    if (!this.durationControl.value) {
      return;
    }

    this.isClassTimeMoreThan40Minutes();
  }

  isClassTimeMoreThan40Minutes(): boolean {
    if (this.durationControl.value > 40) {
      this.toastrComponent.showErrorToast("Health2U - Buscar aulas", `O tempo de aula não pode ultrapassar 40 minutos`, 4000);
      return true;
    }

    return false;
  }

  isDateBeforeNow(control: FormControl): boolean {
    if (moment(control.value) < moment().startOf('day')) {
      this.toastrComponent.showErrorToast("Health2U - Buscar aulas", `A data de início da aula não pode ser antes de agora`, 4000);
      control.setValue(new Date());
      return true;
    }

    return false;
  }

  findClasses() {
    this.classService.getAllFutureLiveClasses()
      .subscribe(payload => {
        const { liveClasses } = payload;

        liveClasses.forEach(liveClass => {
          liveClass['button'] = '#EUQUERO';
        });
        
        this.source.load(liveClasses);
      });
  }

  subscribeClass(classRow: { id: any; title: any; }) {
    if (this.role === 'ROLE_STUDENT') {
      this.authService.getUser();
      const user = this.authService.user;

      const addStudent = {
        liveClassId: classRow.id,
        studentId: user.id
      };
      
      this.classService
        .addStudentToLiveClass(addStudent)
        .toPromise()
        .then(() => {debugger;
          this.toastrComponent.showSuccessToast("Health2U - Aulão", `Você foi inscrito na aula ${classRow.title} com sucesso!`, 4000);
        })
        .catch(error => {debugger;
          this.toastrComponent.showErrorToast("Health2U - Aulão", `Não foi possível te inscrever na aula ${classRow.title}: ${error}`, 4000);
        });

        return;
    }

    this.toastrComponent.showErrorToast("Health2U - Aulão", 'Você não possui o perfil correto para se inscrever em uma aula', 4000);
  }

  // getIdUserFromAuthService() {
  //   this.authService.getToken()
  //     .subscribe((authSimpleToken: AuthSimpleToken) => {
  //       this.userId = authSimpleToken.token.student 
  //         ? authSimpleToken.token.student.id 
  //         : authSimpleToken.token.trainer?.id;
  //     });
  // }
}
