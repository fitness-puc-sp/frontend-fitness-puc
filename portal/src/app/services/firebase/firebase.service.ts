import { ErrorHandler } from './../../utils/error-handler.utils';
import { Injectable, OnInit } from '@angular/core';

// Firebase App (the core Firebase SDK) is always required and must be listed first
import firebase from "firebase/app";
// If you are using v7 or any earlier version of the JS SDK, you should import firebase using namespace import
// import * as firebase from "firebase/app"

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";

@Injectable({
  providedIn: 'root'
})
export class FirebaseService extends ErrorHandler implements OnInit {
  provider: any;

  constructor() { 
    super();

    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
      apiKey: "AIzaSyC9tBshWi9Q3v4IxagRFEJDHJscNTRNBZI",
      authDomain: "fitnesspucjuliano.firebaseapp.com",
      projectId: "fitnesspucjuliano",
      storageBucket: "fitnesspucjuliano.appspot.com",
      messagingSenderId: "761222240978",
      appId: "1:761222240978:web:904afa6a8c02bebf65ab0b",
      measurementId: "G-6807D37S3W"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();

    // Initialize Provider
    this.googleProvider();
  }

  ngOnInit() { }

  googleProvider() {
    // [START auth_google_provider_create]
    this.provider = new firebase.auth.GoogleAuthProvider();
    // [END auth_google_provider_create]
  
    // [START auth_google_provider_scopes]
    // this.provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
    // [END auth_google_provider_scopes]
    
    this.provider.setCustomParameters({
      'login_hint': 'usuário@exemplo.com'
    });
  }

  googleSignInPopup() {
    firebase.auth()
      .signInWithPopup(this.provider)
      .then((result) => {
        /** @type {firebase.auth.OAuthCredential} */
        let credential = result.credential;

        // This gives you a Google Access Token. You can use it to access the Google API.
        // let token = credential.accessToken;
        // The signed-in user info.
        let user = result.user;
        // ...
      }).catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
      });
  }

  googleSignInRedirect() {
    firebase.auth().signInWithRedirect(this.provider);
  }

  signOut() {
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
    }).catch((error) => {
      // An error happened.
    });
  }
}
