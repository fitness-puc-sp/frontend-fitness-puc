import { Injectable } from '@angular/core';
import { NbAuthSimpleToken } from '@nebular/auth';

@Injectable()
export class AuthSimpleToken extends NbAuthSimpleToken {
  token = this.token;
}
