import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbComponentStatus } from '@nebular/theme';
import { environment } from '../../../../environments/environment';
import { Student } from '../../../models/student/student';
import { StudentService } from '../../../services/student/student.service';
import { ToastrComponent } from '../../modal-overlays/toastr/toastr.component';

@Component({
  selector: 'ngx-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
  student = {} as Student;

  fullNameControl = new FormControl(this.student.fullName, [Validators.required, Validators.maxLength(255)]);
  nicknameControl = new FormControl(this.student.nickname, [Validators.required, Validators.maxLength(30)]);
  emailControl = new FormControl(this.student.email, [Validators.required, Validators.email, Validators.maxLength(255)]);
  birthDateControl = new FormControl(this.student.birthDate, [Validators.required]);
  cpfControl = new FormControl(this.student.cpf, [Validators.required, Validators.maxLength(11)]);
  cellphoneControl = new FormControl(this.student.cellphone, [Validators.required, Validators.maxLength(11)]);
  zoomAccountControl = new FormControl(this.student.zoomAccount, [Validators.email, Validators.maxLength(255)]);

  status: NbComponentStatus = 'primary';
  pageTitle: any;

  constructor(
    private route: ActivatedRoute,
    private studentService : StudentService,
    private toastrComponent: ToastrComponent) { 
      
    const id = +this.route.snapshot.paramMap.get('id'); // "+": convert string to number in Javascript

    if (id) {
      this.getStudentById(id);
      this.pageTitle = 'Perfil';
    }
    else {
      this.pageTitle = 'CADASTRO DE ALUNO';
    }
  }

  ngOnInit(): void { }

  getStudentById(id: number): void {
    this.studentService
      .getStudentById(id)
      .subscribe(
        (result:any) => {  // "(result:any)": trecho de código para prevenção do erro de compilação "Property 'student' does not exist on type 'Student'.ts(2339)"
          if (!environment.production) {
            console.log(result);
          }
          
          const {student} = result;   // obtém o objeto "student" do resultado vindo da API
          this.student = student;
          
          // TODO: backend retornar campo active ou filtrar por alunos ativos
          // if (student.active == undefined || !student.active) {
          //   this.showErrorAlert(`Aluno ${this.student.id} não encontrado ou desativado`);
          //   return;
          // }

          this.nicknameControl.setValue(student.nickname);
          this.fullNameControl.setValue(student.fullName);

          const studentBirthDate = new Date(Date.parse(student.birthDate));
          const ano = studentBirthDate.getFullYear();
          const correctMonth = (studentBirthDate.getMonth() + 1);
          const mes = correctMonth > 10 ? correctMonth : `0${correctMonth}`;
          const dia = studentBirthDate.getDate() > 10 ? studentBirthDate.getDate() : `0${studentBirthDate.getDate()}`;
          const studentBirthDateToString = `${ano}-${mes}-${dia}`;
          this.birthDateControl.setValue(studentBirthDateToString);
          
          this.cpfControl.setValue(student.cpf);
          this.emailControl.setValue(student.email);
          this.cellphoneControl.setValue(student.cellphone);
          this.zoomAccountControl.setValue(student.zoomAccount);
          
          // TODO: trabalhar na desabilitação dos campos não editáveis
          // this.cpfEnabled = false;
        },
        (error) => {
          this.toastrComponent.showErrorToast("Health2U - Alunos", `Não foi possível obter o aluno ${this.student.fullName}: ${error}`, 4000);
        });
  }

  save() {
    this.student.nickname = this.nicknameControl.value;
    this.student.fullName = this.fullNameControl.value;
    this.student.birthDate = this.birthDateControl.value;
    this.student.cpf = this.cpfControl.value;
    this.student.email = this.emailControl.value;
    this.student.cellphone = this.cellphoneControl.value;
    this.student.zoomAccount = this.zoomAccountControl.value;

    if (this.isTransient()) {
      this.studentService
        .saveStudent(this.student)
        .toPromise()
        .then(() => {
          this.toastrComponent.showSuccessToast("Health2U - Alunos", `O aluno ${this.student.fullName} foi salvo com sucesso!`, 4000);
          this.resetForm();
        })
        .catch(error => {
          this.toastrComponent.showErrorToast("Health2U - Alunos", `Não foi possível salvar o aluno ${this.student.fullName}: ${error}`, 4000);
        });

      return;
    }

    this.studentService
      .updateStudent(this.student)
      .toPromise()
      .then(() => {
        this.toastrComponent.showSuccessToast("Health2U - Alunos", `O aluno ${this.student.fullName} foi salvo com sucesso!`, 4000);
        this.resetForm();
      })
      .catch(error => {
        this.toastrComponent.showErrorToast("Health2U - Alunos", `Não foi possível salvar o aluno ${this.student.fullName}: ${error}`, 4000);
    });
  }

  resetForm() {
    this.nicknameControl.setValue("");
    this.fullNameControl.setValue("");
    this.birthDateControl.setValue("");
    this.cpfControl.setValue("");
    this.emailControl.setValue("");
    this.cellphoneControl.setValue("");
    this.zoomAccountControl.setValue("");
  }

  isTransient(): boolean {
    if (!this.student.id) {
      return true;
    }

    return this.student.id === 0;
  }

  getRequiredErrorMessage(formControl: FormControl, formControlName: string) {
    return formControl.hasError('required') ? `Por favor insira um(a) ${formControlName} válido(a)'` : '';
  }

  getEmailValidatorMessage(formControl: FormControl, formControlName: string) {
    return formControl.hasError('email') ? `${formControlName} inválido(a)` : '';
  }  



  getEmailErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.emailControl, 'e-mail')} / 
      ${this.getEmailValidatorMessage(this.emailControl, 'e-mail')}
    `;
  }

  getNicknameErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.nicknameControl, 'apelido')}
    `;
  }

  getFullNameErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.fullNameControl, 'nome completo')}
    `;
  }

  getBirthDateErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.birthDateControl, 'data de nascimento')}
    `;
  }

  getCpfErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.cpfControl, 'CPF')}
    `;
  }

  getCellphoneErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.cellphoneControl, 'celular')}
    `;
  }

  getZoomAccountErrorMessages() {
    return `
      ${this.getEmailValidatorMessage(this.zoomAccountControl, 'conta do Zoom')}
    `;
  }
}
