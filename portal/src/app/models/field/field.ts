import { Category } from "../category/category";

export interface Field {
    id: number;
    name: string;
    category: Category;
    certified: boolean;
}