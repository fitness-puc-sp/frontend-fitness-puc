export interface LiveClass {
    id?: number;
    startTime: string;
    duration: number;
    trainer: number;
    field: number;
    title: string;
    type: string;
    description: string;
    startUrl: string;
    joinUrl: string;
}
