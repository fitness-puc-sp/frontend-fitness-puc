export class DateHandler {
    static getStringNowDateForDateTimeInput(): string {
        const now = new Date();
        const month = now.getMonth() + 1;
        const stringMonth = month < 10 ? `0${month}` : month;
        const day = now.getDate();
        const stringDay = day < 10 ? `0${day}` : day;
        const hours = now.getHours();
        const stringHours = hours < 10 ? `0${hours}` : hours;
        const mins = now.getMinutes();
        const stringMins = mins < 10 ? `0${mins}` : mins;
    
        const formattedDate = `${now.getFullYear()}-${stringMonth}-${stringDay}T${stringHours}:${stringMins}`;
        return formattedDate;
    }
}
