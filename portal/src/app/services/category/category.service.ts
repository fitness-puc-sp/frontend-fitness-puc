import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Category } from '../../models/category/category';
import { Field } from '../../models/field/field';
import { ErrorHandler } from '../../utils/error-handler.utils';

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends ErrorHandler {

  constructor(private http: HttpClient) { 
    super(); 
  }

  // saveCategory(category: Category): Observable<Category> {
  //   return this.http
  //     .post<Category>(`${environment.apiUrl}/category`, category)
  //     .pipe(
  //       tap(_ => {
  //         if (!environment.production) {
  //           console.log(_);
  //         }
  //       }),
  //       catchError(this.handleError)
  //     );
  // }
  
  // updateCategory(category: Category): Observable<Category> {
  //   return this.http
  //     .put<Category>(`${environment.apiUrl}/category`, category)
  //     .pipe(
  //       tap(_ => {
  //         if (!environment.production) {
  //           console.log(_);
  //         }
  //       }),
  //       catchError(this.handleError)
  //     );
  // }

  // deleteCategory(id: number): Observable<any> {
  //   return this.http
  //     .delete(`${environment.apiUrl}/category/${id}`)
  //     .pipe(
  //       tap(_ => {
  //         if (!environment.production) {
  //           console.log(_);
  //         }
  //       }),
  //       catchError(this.handleError)
  //     );
  // }

  //#region "Gets for categories"
  getAllCategories(): Observable<any> {
    const url = `${environment.apiUrl}/category`;
    return this.http.get(url);
  }

  getCategoryById(id: number): Observable<Category> {
    const url = `${environment.apiUrl}/category/id/${id}`;
    return this.http.get<Category>(url);
  }
  //#endregion

  //#region "Gets for fields by category"
  getFieldsByCategoryId(categoryId: number): Observable<any> {
    const url = `${environment.apiUrl}/category/id/${categoryId}/fields`;
    return this.http.get<Field>(url);
  }
  //#endregion
}
