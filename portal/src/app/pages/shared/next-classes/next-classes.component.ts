import { AuthService } from './../../../auth/auth.service';
import { ClassService } from './../../../services/class/class.service';
import { RoleProvider } from './../../../auth/role.provider';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { Subject } from 'rxjs';
import {
  startOfDay,
  endOfDay,
  isSameDay,
  isSameMonth,
  addMinutes,
} from 'date-fns';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'ngx-next-classes',
  templateUrl: './next-classes.component.html',
  styleUrls: ['./next-classes.component.scss']
})
export class NextClassesComponent implements OnInit {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  refresh: Subject<any> = new Subject();
  events: CalendarEvent[] = [];
  activeDayIsOpen: boolean = true;
  showCreateLiveClass: boolean;
  liveClasses: any;
  user: any;
  userRole: any;
  urlClass: any;

  calendarPreviousViewTitle = 'Mês anterior';
  calendarActualViewTitle = 'Mês atual';
  calendarNextViewTitle = 'Próximo mês';

  constructor(
    private roleProvider: RoleProvider,
    private classService: ClassService,
    private authService: AuthService) {
      this.showCreateLiveClassByRole();
      this.getUserId();
      this.getLiveClasses();
    }

  ngOnInit() { }

  populateEventsWithClasses() {
    this.liveClasses.forEach(liveClass => {
      const startDateTime = new Date(liveClass.startTime[0], liveClass.startTime[1]-1, liveClass.startTime[2], liveClass.startTime[3], liveClass.startTime[4]);
      let actions: CalendarEventAction[];

      if (this.userRole === 'ROLE_TRAINER') {
        actions = [
          {
            label: '<i class="fas fa-bullhorn" style="color: white;!important"></i>',
            a11yLabel: 'StartClass',
            onClick: ({ event }: { event: CalendarEvent }): void => {
              // this.handleEvent('StartClass', event);
              window.open(this.urlClass, '_blank');
            },
          },
        ];

        if (!liveClass.startUrl) {
          liveClass.startUrl = 'Sua aula ainda não foi criada!';
        }  
  
        this.urlClass = liveClass.startUrl;
      } else {
        actions = [          
          {
            label: '<i class="fas fa-dumbbell" style="color: white;!important"></i>',
            a11yLabel: 'JoinClass',
            onClick: ({ event }: { event: CalendarEvent }): void => {
              // this.events = this.events.filter((iEvent) => iEvent !== event);
              // this.handleEvent('JoinClass', event);
              window.open(this.urlClass, '_blank');
            },
          },
        ];

        if (!liveClass.joinUrl) {
          liveClass.joinUrl = 'Sua aula ainda não começou!';
        }
  
        this.urlClass = liveClass.joinUrl;
      }  

      var keys = Object.keys(colors);
      const eventColor = colors[keys[ keys.length * Math.random() << 0]];
      
      this.events.push({
        start: startDateTime,
        end: addMinutes(startDateTime, +liveClass.duration),
        title: `${liveClass.title}`,
        color: eventColor,
        actions: actions,
      });
    });
  }

  getUserId() {
    this.authService.getUser();
    this.user = this.authService.user;
  }

  private getLiveClasses() {
    this.classService.getAllFutureLiveClasses()
      .subscribe(payload => {
        const { liveClasses } = payload;
        this.liveClasses = liveClasses.filter((liveClass: { students: any[]; }) => {
          return liveClass.students.filter((student) => {
            return student.id === this.user.id;
          })
        });

        this.populateEventsWithClasses();
      });
  }

  showCreateLiveClassByRole() {
    this.roleProvider.getRole()
      .subscribe(role => {
        this.userRole = role;
        this.showCreateLiveClass = role === 'ROLE_TRAINER';
      });
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {debugger;
    // this.modalData = { event, action };
    // this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {debugger;
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true,
        },
      },
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {debugger;
    this.events = this.events.filter((event) => event !== eventToDelete);
  }

  setView(view: CalendarView) {debugger;
    this.view = view;

    this.setCalendarPreviousViewTitle(view);
    this.setCalendarActualViewTitle(view);
    this.setCalendarNextViewTitle(view);
  }

  private setCalendarPreviousViewTitle(view: CalendarView) {
    switch (view) {
      case CalendarView.Day:
        this.calendarPreviousViewTitle = 'Dia';
        break;
      case CalendarView.Week:
        this.calendarPreviousViewTitle = 'Semana';
        break;
      case CalendarView.Month:
        this.calendarPreviousViewTitle = 'Mês';
        break;
    }

    this.calendarPreviousViewTitle += ' anterior';
  }

  private setCalendarActualViewTitle(view: CalendarView) {
    switch (view) {
      case CalendarView.Day:
        this.calendarActualViewTitle = 'Dia';
        break;
      case CalendarView.Week:
        this.calendarActualViewTitle = 'Semana';
        break;
      case CalendarView.Month:
        this.calendarActualViewTitle = 'Mês';
        break;
    }

    this.calendarActualViewTitle += ' atual';
  }

  private setCalendarNextViewTitle(view: CalendarView) {
    switch (view) {
      case CalendarView.Day:
        this.calendarNextViewTitle = 'Próximo dia';
        break;
      case CalendarView.Week:
        this.calendarNextViewTitle = 'Próxima semana';
        break;
      case CalendarView.Month:
        this.calendarNextViewTitle = 'Próximo mês';
        break;
    }
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
}
