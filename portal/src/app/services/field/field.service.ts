import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ErrorHandler } from '../../utils/error-handler.utils';
import { CategoryService } from '../category/category.service';

@Injectable({
  providedIn: 'root'
})
export class FieldService extends ErrorHandler {
  
  constructor(
    private categoryService: CategoryService) { 
    super(); 
  }

  //#region "Gets for fields"
  getAllFieldsByCategoryId(categoryId: number): Observable<any> {
    return this.categoryService.getFieldsByCategoryId(categoryId);
  }
  //#endregion
}
