import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbComponentStatus } from '@nebular/theme';
import { environment } from '../../../../environments/environment';
import { LiveClass } from '../../../models/live-class/live-class';
import { CategoryService } from '../../../services/category/category.service';
import { FieldService } from '../../../services/field/field.service';
import { ToastrComponent } from '../../modal-overlays/toastr/toastr.component';
import * as moment from 'moment';
import { DateHandler } from '../../../utils/date-handler.util';
import { ClassService } from '../../../services/class/class.service';
import { AuthService } from '../../../auth/auth.service';
import { RoleProvider } from '../../../auth/role.provider';

@Component({
  selector: 'ngx-live-class',
  templateUrl: './live-class.component.html',
  styleUrls: ['./live-class.component.scss']
})
export class LiveClassComponent implements OnInit {

  liveclass = {} as LiveClass;

  titleControl = new FormControl(this.liveclass.title, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]);
  descriptionControl = new FormControl(this.liveclass.description, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]);
  startTimeControl = new FormControl(this.liveclass.startTime, [Validators.required]);
  durationControl = new FormControl(this.liveclass.duration, [Validators.required, Validators.min(5), Validators.max(40)]);
  typeControl = new FormControl(this.liveclass.type, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]);
  categoriesControl = new FormControl(null, [Validators.required]);
  fieldsControl = new FormControl(null, [Validators.required]);

  status: NbComponentStatus = 'primary';

  categories: any;
  fields: any;
  selectedCategory = '1';
  selectedField = '1';
  minDateTime: any;
  role: any;

  constructor(
    private route: ActivatedRoute,
    private classService : ClassService,
    private categoryService: CategoryService,
    private fieldService: FieldService,
    private toastrComponent: ToastrComponent,
    private authService: AuthService,
    private roleProvider: RoleProvider) { 
      
    const id = +this.route.snapshot.paramMap.get('id'); // "+": convert string to number in Javascript

    if (id) {
      this.getLiveClassById(id);
    }

    this.getRole();
  }

  getRole() {
    this.roleProvider.getRole()
      .subscribe(role => {
        this.role = role;
      });  
  }

  ngOnInit(): void {
    this.minDateTime = DateHandler.getStringNowDateForDateTimeInput();
    this.getCategories();
  }

  getLiveClassById(id: number): void {
    this.classService
      .getLiveClassById(id)
      .subscribe(
        (result:any) => {  // "(result:any)": trecho de código para prevenção do erro de compilação "Property 'Open Class' does not exist on type 'Open Class'.ts(2339)"
          if (!environment.production) {
            console.log(result);
          }
          
          this.liveclass = result.liveClass;
          
          const { liveClass } = result;   // obtém o objeto "liveclass" do resultado vindo da API

          this.titleControl.setValue(liveClass.title);
          this.descriptionControl.setValue(liveClass.description);
          this.startTimeControl.setValue(liveClass.startTime);
          this.durationControl.setValue(liveClass.duration);
          this.fieldsControl.setValue(liveClass.field);
          this.typeControl.setValue(liveClass.type);
        },
        (error) => {
          this.toastrComponent.showErrorToast("Health2U - Aulão", `Não foi possível obter o aulão ${this.liveclass.title}: ${error}`, 4000);
        });
  }

  save() {
    if (!this.userHasTrainerRole()) {
      this.toastrComponent.showErrorToast("Health2U - Aulão", 'Você não possui o perfil correto para se criar uma aula', 4000);
      return;
    }

    if (this.isStartTimeBeforeNow()) {
      return;
    }

    if (this.isClassTimeMoreThan40Minutes()) {
      return;
    }

    this.liveclass.title = this.titleControl.value;
    this.liveclass.description = this.descriptionControl.value;
    this.liveclass.startTime = this.startTimeControl.value;
    this.liveclass.duration = this.durationControl.value;
    this.liveclass.field = +this.fieldsControl.value;
    this.liveclass.type = this.typeControl.value;

    this.authService.getUser();
    this.liveclass.trainer = this.authService.user.id;

    if (this.isTransient()) {
      this.classService
        .saveLiveClass(this.liveclass)
        .toPromise()
        .then(() => {
          this.toastrComponent.showSuccessToast("Health2U - Aulão", `O aulão ${this.liveclass.title} foi salvo com sucesso!`, 4000);
          this.resetForm();
        })
        .catch(error => {
          this.toastrComponent.showErrorToast("Health2U - Aulão", `Não foi possível salvar o aulão ${this.liveclass.title}: ${error}`, 4000);
        });

      return;
    }

    this.classService
      .updateLiveClass(this.liveclass)
      .toPromise()
      .then(() => {
        this.toastrComponent.showSuccessToast("Health2U - Aulão", `O aulão ${this.liveclass.title} foi salvo com sucesso!`, 4000);
        this.resetForm();
      })
      .catch(error => {
        this.toastrComponent.showErrorToast("Health2U - Aulão", `Não foi possível salvar o aulão ${this.liveclass.title}: ${error}`, 4000);
    });
  }

  userHasTrainerRole(): boolean {
    return this.role === 'ROLE_TRAINER';
  }

  resetForm() {
    this.titleControl.setValue("");
    this.descriptionControl.setValue("");
    this.startTimeControl.setValue("");
    this.durationControl.setValue("");
    this.categoriesControl.setValue("");
    this.fieldsControl.setValue("");
    this.typeControl.setValue("");
  }

  isTransient(): boolean {
    if (!this.liveclass.id) {
      return true;
    }

    return this.liveclass.id === 0;
  }

  getCategories() {
    this.categoryService.getAllCategories()
      .subscribe(payload => {
        this.categories = payload.categories;

        if (payload.categories.length === 1) {
          this.selectedCategory = payload.categories[0].id;
        }
      });
  }

  getAllFieldsByCategoryId(categoryId: number) {
    this.fieldService.getAllFieldsByCategoryId(categoryId)
      .subscribe(payload => {
        this.fields = payload.fields;

        if (payload.fields.length === 1) {
          this.selectedField = payload.fields[0].id;
        }
      });
  }

  categoryChanged(event:any) {
    this.fields = null;
    this.selectedField = null;
    this.getAllFieldsByCategoryId(+event);
  }

  onStartTimeBlur(event) {
    if (this.startTimeControl.value && this.isStartTimeBeforeNow()) {
      return;
    }
  }

  onDurationBlur(event) {
    if (!this.durationControl.value) {
      return;
    }

    this.isClassTimeMoreThan40Minutes();
  }

  isClassTimeMoreThan40Minutes(): boolean {
    if (this.durationControl.value > 40) {
      this.toastrComponent.showErrorToast("Health2U - Aulão", `O tempo de aula não pode ultrapassar 40 minutos`, 4000);
      return true;
    }

    return false;
  }

  isStartTimeBeforeNow(): boolean {
    const startDateTime = new Date(this.startTimeControl.value);

    if (moment(startDateTime).isBefore(new Date())) {
      this.toastrComponent.showErrorToast("Health2U - Aulão", `O início do aulão não pode ser antes de agora`, 4000);
      this.startTimeControl.setValue(new Date());
      return true;
    }

    return false;
  }
}
