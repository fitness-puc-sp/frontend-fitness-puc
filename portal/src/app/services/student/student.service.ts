import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ErrorHandler } from '../../utils/error-handler.utils';
import { Student } from '../../models/student/student';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentService extends ErrorHandler {
  studentRoute = 'student';

  constructor(private http: HttpClient) { 
    super(); 
  }

  saveStudent(student: Student): Observable<Student> {
    return this.http
      .post<Student>(`${environment.apiUrl}/${this.studentRoute}`, student)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }
  
  updateStudent(student: Student): Observable<Student> {
    return this.http
      .put<Student>(`${environment.apiUrl}/${this.studentRoute}`, student)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }

  deleteStudent(id: number): Observable<any> {
    return this.http
      .delete(`${environment.apiUrl}/${this.studentRoute}/${id}`)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }

  //#region "Gets for students"
  getStudentByName(name: string): Observable<any> {
    const url = `${environment.apiUrl}/${this.studentRoute}/name/${name}`;
    return this.http.get(url);
  }

  getAllStudents(): Observable<any> {
    const url = `${environment.apiUrl}/${this.studentRoute}`;
    return this.http.get(url);
  }

  getStudentById(id: number): Observable<Student> {
    const url = `${environment.apiUrl}/${this.studentRoute}/id/${id}`;
    return this.http.get<Student>(url);
  }
  //#endregion
}
