import { Environment } from "../app/models/environment/environment";

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment: Environment = {
  production: true,
  apiUrl: "http://34.70.235.9/virtual-trainer"  
};
