import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';

import { NbAuthService } from '@nebular/auth';
import { NbRoleProvider } from '@nebular/security';
import { AuthSimpleToken } from './auth-simple-token';

@Injectable({
  providedIn: 'root'
})
export class RoleProvider implements NbRoleProvider {

  constructor(private authService: NbAuthService) { }

  getRole(): Observable<string> {
    return this.authService.onTokenChange()
      .pipe(
        map((authSimpleToken: AuthSimpleToken) => {
          const role = authSimpleToken.token.authorities[0].authority;
          return authSimpleToken.isValid() ? role : 'guest';
        }),
      );
  }
}
