import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NextClassesComponent } from './next-classes.component';

describe('NextClassesComponent', () => {
  let component: NextClassesComponent;
  let fixture: ComponentFixture<NextClassesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NextClassesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NextClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
