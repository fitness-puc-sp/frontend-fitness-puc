import { NextClassesComponent } from './next-classes/next-classes.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LiveClassComponent } from './live-class/live-class.component';
import { SharedComponent } from './shared.component';
import { StudentComponent } from './student/student.component';
import { TrainerComponent } from './trainer/trainer.component';
import { FindClassComponent } from './find-class/find-class.component';

const routes: Routes = [
  {
    path: '',
    component: SharedComponent,
    children: [
      {
        path: 'next-classes',
        component: NextClassesComponent,
      },
      {
        path: 'find-class',
        component: FindClassComponent,
      },
      {
        path: 'live-class',
        component: LiveClassComponent,
      },
      {
        path: 'live-class/:id',
        component: LiveClassComponent,
      },
      {
        path: 'student',
        component: StudentComponent,
      },
      {
        path: 'trainer',
        component: TrainerComponent,
      },
      {
        path: 'student/:id',
        component: StudentComponent,
      },
      {
        path: 'trainer/:id',
        component: TrainerComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class SharedRoutingModule {
}
