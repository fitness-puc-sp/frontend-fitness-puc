export interface Student {
    id?: number;
    nickname: string;
    fullName: string;
    birthDate: string;
    cpf: number;
    email: string;
    cellphone: number;
    zoomAccount: string;
    active?: boolean;
}