import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbComponentStatus } from '@nebular/theme';
import { environment } from '../../../../environments/environment';
import { Trainer } from '../../../models/trainer/trainer';
import { TrainerService } from '../../../services/trainer/trainer.service';
import { ToastrComponent } from '../../modal-overlays/toastr/toastr.component';

@Component({
  selector: 'ngx-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {
  trainer = {} as Trainer;
  
  fullNameControl = new FormControl(this.trainer.fullName, [Validators.required, Validators.maxLength(255)]);
  nicknameControl = new FormControl(this.trainer.nickname, [Validators.required, Validators.maxLength(30)]);
  emailControl = new FormControl(this.trainer.email, [Validators.required, Validators.email, Validators.maxLength(255)]);
  birthDateControl = new FormControl(this.trainer.birthdate, [Validators.required]);
  cpfControl = new FormControl(this.trainer.cpf, [Validators.required, Validators.maxLength(11)]);
  cnpjControl = new FormControl(this.trainer.cnpj, [Validators.required, Validators.maxLength(14)]);
  cellphoneControl = new FormControl(this.trainer.cellphone, [Validators.required, Validators.maxLength(11)]);
  zoomAccountControl = new FormControl(this.trainer.zoomAccount, [Validators.email, Validators.maxLength(255)]);

  status: NbComponentStatus = 'primary';
  pageTitle: any;

  constructor(
    private route: ActivatedRoute,
    private trainerService : TrainerService,
    private toastrComponent: ToastrComponent) {

    const id = +this.route.snapshot.paramMap.get('id'); // "+": convert string to number in Javascript

    if (id) {
      this.getTrainerById(id);
      this.pageTitle = 'Perfil';
    }
    else {
      this.pageTitle = 'CADASTRO DE PROFISSIONAL';
    }
  }

  ngOnInit(): void { }

  getTrainerById(id: number): void {
    this.trainerService
      .getTrainerById(id)
      .subscribe(
        (result:any) => {  // "(result:any)": trecho de código para prevenção do erro de compilação "Property 'trainer' does not exist on type 'Trainer'.ts(2339)"
          if (!environment.production) {
            console.log(result);
          }

          const {trainer} = result;   // obtém o objeto "trainer" do resultado vindo da API
          this.trainer = trainer;

          this.nicknameControl.setValue(trainer.nickname);
          this.fullNameControl.setValue(trainer.fullName);

          const trainerBirthDate = new Date(Date.parse(trainer.birthDate));
          const ano = trainerBirthDate.getFullYear();
          const correctMonth = (trainerBirthDate.getMonth() + 1);
          const mes = correctMonth > 10 ? correctMonth : `0${correctMonth}`;
          const dia = trainerBirthDate.getDate()> 10 ? trainerBirthDate.getDate() : `0${trainerBirthDate.getDate()}`;
          const trainerBirthDateToString = `${ano}-${mes}-${dia}`;
          this.birthDateControl.setValue(trainerBirthDateToString);
        
          this.cpfControl.setValue(trainer.cpf);
          this.cnpjControl.setValue(trainer.cnpj);
          this.emailControl.setValue(trainer.email);
          this.cellphoneControl.setValue(trainer.cellphone);
          this.zoomAccountControl.setValue(trainer.zoomAccount);

          // TODO: trabalhar na desabilitação dos campos não editáveis
          // this.cpfEnabled = false;
        },
        (error) => {
          this.toastrComponent.showErrorToast("Health2U - Profissionais", `Não foi possível obter o profissional: ${error}`, 4000);
        });
  }

  save() {
    this.trainer.fullName = this.fullNameControl.value;
    this.trainer.nickname = this.nicknameControl.value;
    this.trainer.birthdate = this.birthDateControl.value;
    this.trainer.cpf = this.cpfControl.value;
    this.trainer.cnpj = this.cnpjControl.value;
    this.trainer.email = this.emailControl.value;
    this.trainer.cellphone = this.cellphoneControl.value;
    this.trainer.zoomAccount = this.zoomAccountControl.value;

    if (this.isTransient()) {
      this.trainerService
        .saveTrainer(this.trainer)
        .toPromise()
        .then(() => {
          this.toastrComponent.showSuccessToast("Health2U - Profissionais", `O profissional ${this.trainer.fullName} foi salvo com sucesso!`, 4000);
          this.resetForm();
        })
        .catch(error => {
          this.toastrComponent.showErrorToast("Health2U - Profissionais", `Não foi possível salvar o profissional ${this.trainer.fullName}: ${error}`, 4000);
        });

      return;
    }

    this.trainerService
      .updateTrainer(this.trainer)
      .toPromise()
      .then(() => {
        this.toastrComponent.showSuccessToast("Health2U - Profissionais", `O profissional ${this.trainer.fullName} foi salvo com sucesso!`, 4000);
        this.resetForm();
      })
      .catch(error => {
        this.toastrComponent.showErrorToast("Health2U - Profissionais", `Não foi possível salvar o profissional ${this.trainer.fullName}: ${error}`, 4000);
      });
  }

  resetForm() {
    this.nicknameControl.setValue("");
    this.fullNameControl.setValue("");
    this.birthDateControl.setValue("");
    this.cpfControl.setValue("");
    this.cnpjControl.setValue("");
    this.emailControl.setValue("");
    this.cellphoneControl.setValue("");
    this.zoomAccountControl.setValue("");
  }

  isTransient(): boolean {
    if (!this.trainer.id) {
      return true;
    }

    return this.trainer.id === 0;
  }

  getRequiredErrorMessage(formControl: FormControl, formControlName: string) {
    return formControl.hasError('required') ? `Por favor insira um(a) ${formControlName} válido(a)'` : '';
  }

  getEmailValidatorMessage(formControl: FormControl, formControlName: string) {
    return formControl.hasError('email') ? `${formControlName} inválido(a)` : '';
  }




  getEmailErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.emailControl, 'e-mail')} /
      ${this.getEmailValidatorMessage(this.emailControl, 'e-mail')}
    `;
  }

  getNicknameErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.nicknameControl, 'apelido')}
    `;
  }

  getFullNameErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.fullNameControl, 'nome completo')}
    `;
  }

  getBirthDateErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.birthDateControl, 'data de nascimento')}
    `;
  }

  getCpfErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.cpfControl, 'CPF')}
    `;
  }

  getCnpjErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.cnpjControl, 'CNPJ')}
    `;
  }

  getCellphoneErrorMessages() {
    return `
      ${this.getRequiredErrorMessage(this.cellphoneControl, 'celular')}
    `;
  }

  getZoomAccountErrorMessages() {
    return `
      ${this.getEmailValidatorMessage(this.zoomAccountControl, 'conta do Zoom')}
    `;
  }
}
