import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { LiveClass } from '../../models/live-class/live-class';
import { ErrorHandler } from '../../utils/error-handler.utils';

@Injectable({
  providedIn: 'root'
})
export class ClassService extends ErrorHandler {
  liveClassRoute = 'live-class';

  constructor(private http: HttpClient) { 
    super(); 
  }

  saveLiveClass(liveclass: LiveClass): Observable<LiveClass> {
    return this.http
      .post<LiveClass>(`${environment.apiUrl}/${this.liveClassRoute}`, liveclass)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }

  updateLiveClass(liveclass: LiveClass): Observable<LiveClass> {
    return this.http
      .patch<LiveClass>(`${environment.apiUrl}/${this.liveClassRoute}`, liveclass)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }		

  deleteLiveClass(id: number): Observable<any> {
    return this.http
      .delete(`${environment.apiUrl}/${this.liveClassRoute}/${id}`)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }

  addStudentToLiveClass(addStudentToLiveClass: AddStudent): Observable<any> {
    return this.http
      .post<AddStudent>(`${environment.apiUrl}/${this.liveClassRoute}/addStudent`, addStudentToLiveClass)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log(_);
          }
        }),
        catchError(this.handleError)
      );
  }		

  //#region "Gets for live classes"
  getLiveClassByTitle(title: string): Observable<any> {
    const url = `${environment.apiUrl}/${this.liveClassRoute}/title/${title}`;
    return this.http.get(url);
  }

  getAllFutureLiveClasses(): Observable<any> {
    const url = `${environment.apiUrl}/${this.liveClassRoute}`;
    return this.http.get(url);
  }

  getLiveClassById(id: number): Observable<LiveClass> {
    const url = `${environment.apiUrl}/${this.liveClassRoute}/id/${id}`;
    return this.http.get<LiveClass>(url);
  }
  //#endregion
}

export interface AddStudent {
  liveClassId: number;
  studentId: number;
}
