import { RoleProvider } from './../auth/role.provider';
import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {

  menu = MENU_ITEMS;

  constructor(private roleProvider: RoleProvider) { 
    this.filterMenuByRole(roleProvider);
  }

  private filterMenuByRole(roleProvider: RoleProvider) {
    roleProvider.getRole()
      .subscribe(role => {
        if (role !== 'ROLE_TRAINER') {
          this.menu = this.menu.filter(function (menuItem) {
            return menuItem.title !== 'Criar aulões';
          });
        }
      });
  }
}
